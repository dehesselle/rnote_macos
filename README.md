# Rnote for macOS

![GitLab master branch](https://gitlab.com/dehesselle/rnote_macos/badges/master/pipeline.svg)
![Latest Release](https://img.shields.io/gitlab/v/release/dehesselle/rnote_macos?sort=semver&color=2f699b&label=Latest%20Release)

![rnote](resources/rnote_macos.png)

This is the official macOS release for [Rnote](https://rnote.flxzt.net). The app is standalone, relocatable and supports macOS Catalina up to macOS Sonoma.

## download

Downloads are available in the [Releases](https://gitlab.com/dehesselle/rnote_macos/-/releases) section.

## contact

Please use the official channels:

- report issues to Rnote's [issue tracker](https://github.com/flxzt/rnote/issues)
- ask questions in Rnote's [Discussions](https://github.com/flxzt/rnote/discussions)
- chat with us on [#rnote:matrix.org](https://matrix.to/#/#rnote:matrix.org)

## license

This work is licensed under [GPL-2.0-or-later](LICENSE).  
Rnote is licensed under [GPL-3.0-or-later](https://github.com/zim-desktop-wiki/zim-desktop-wiki#copyright-and-license).

### additional credits

Built using other people's work:

- [gtk-osx](https://gitlab.gnome.org/GNOME/gtk-osx) licensed under GPL-2.0-or-later.
