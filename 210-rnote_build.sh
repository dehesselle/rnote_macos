#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Build and install Rnote to VER_DIR.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

### variables ##################################################################

# Nothing here.

### functions ##################################################################

# Nothing here.

### main #######################################################################

error_trace_enable

#------------------------------------------------------------------- build Rnote

if [ -d "$RNOTE_SRC_DIR/.git" ]; then
  git -C "$RNOTE_SRC_DIR" reset --hard HEAD
  git -C "$RNOTE_SRC_DIR" fetch
else
  git clone "$RNOTE_URL" "$RNOTE_SRC_DIR"
fi

git -C "$RNOTE_SRC_DIR" checkout "$RNOTE_VERSION"
git -C "$RNOTE_SRC_DIR" submodule update --init --recursive

# add build number to Rnote version
gsed -i "s/\(patch\ = '[^']*\)/\1 ($RNOTE_BUILD)/" "$RNOTE_SRC_DIR"/meson.build

jhb run meson setup \
  --prefix "$VER_DIR" \
  -Dcli=true \
  "$RNOTE_BLD_DIR" \
  "$RNOTE_SRC_DIR"

jhb run meson compile -C "$RNOTE_BLD_DIR"
jhb run meson install -C "$RNOTE_BLD_DIR"
