# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# This file contains settings and functions specific to Rnote.

### shellcheck #################################################################

# shellcheck shell=bash # no shebang as this file is intended to be sourced
# shellcheck disable=SC2034 # no exports desired

### dependencies ###############################################################

# Nothing here.

### variables ##################################################################

RNOTE_SRC_DIR=$SRC_DIR/rnote
RNOTE_BLD_DIR=$BLD_DIR/rnote

RNOTE_APP_DIR=$ART_DIR/Rnote.app
RNOTE_APP_CON_DIR=$RNOTE_APP_DIR/Contents
RNOTE_APP_RES_DIR=$RNOTE_APP_CON_DIR/Resources
RNOTE_APP_BIN_DIR=$RNOTE_APP_RES_DIR/bin
RNOTE_APP_ETC_DIR=$RNOTE_APP_RES_DIR/etc
RNOTE_APP_LIB_DIR=$RNOTE_APP_RES_DIR/lib
RNOTE_APP_FRA_DIR=$RNOTE_APP_CON_DIR/Frameworks
RNOTE_APP_PLIST=$RNOTE_APP_CON_DIR/Info.plist

RNOTE_BUILD=${RNOTE_BUILD:-0}

RNOTE_VERSION=${RNOTE_VERSION:-v0.11.0}
RNOTE_URL=https://github.com/flxzt/rnote

### functions ##################################################################

function rnote_get_version_from_module
{
  xmllint \
    --xpath "string(//moduleset/meson[@id='rnote']/branch/@version)" \
    "$SELF_DIR"/modulesets/rnote.modules
}

function rnote_get_version_from_meson
{
  grep version "$RNOTE_SRC_DIR"/meson.build |
    head -1 |
    awk -F "'" '{ print $2 }'
}

function rnote_get_version_from_plist
{
  /usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" \
    "$RNOTE_APP_PLIST"
}

### main #######################################################################

# Nothing here.
