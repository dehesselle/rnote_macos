#!/usr/bin/env bash
#
# SPDX-FileCopyrightText: 2022 René de Hesselle <dehesselle@web.de>
#
# SPDX-License-Identifier: GPL-2.0-or-later

### description ################################################################

# Create the Rnote application bundle.

### shellcheck #################################################################

# Nothing here.

### dependencies ###############################################################

source "$(dirname "${BASH_SOURCE[0]}")"/jhb/etc/jhb.conf.sh

### variables ##################################################################

SELF_DIR=$(dirname "$(greadlink -f "$0")")

### functions ##################################################################

# Nothing here.

### main #######################################################################

error_trace_enable

#----------------------------------------------------------------- prepare icons

svg2icns \
  "$RNOTE_SRC_DIR"/crates/rnote-ui/data/icons/scalable/apps/rnote-devel.svg \
  "$TMP_DIR"/Rnote.icns

#----------------------------------------------------- create application bundle

(
  cd "$SELF_DIR" || exit 1
  export ART_DIR # is referenced in rnote.bundle

  jhb run gtk-mac-bundler resources/rnote.bundle
)

lib_change_path @executable_path/../Resources/lib/libpoppler.139.dylib \
  "$RNOTE_APP_CON_DIR"/MacOS/Rnote
lib_change_path @executable_path/../Resources/lib/libpoppler-glib.8.dylib \
  "$RNOTE_APP_CON_DIR"/MacOS/Rnote

# Libraries in RNOTE_APP_LIB_DIR can reference each other directly.
lib_change_siblings "$RNOTE_APP_LIB_DIR"

# Adjust library paths for other binaries.
lib_change_paths @executable_path/../Resources/lib "$RNOTE_APP_LIB_DIR" \
  "$RNOTE_APP_CON_DIR/MacOS/rnote-cli \
$RNOTE_APP_CON_DIR/MacOS/gtk4-demo \
$RNOTE_APP_CON_DIR/MacOS/adwaita-1-demo \
"

#------------------------------------------------------------- update Info.plist

# enable HiDPI
/usr/libexec/PlistBuddy -c "Add NSHighResolutionCapable bool 'true'" \
  "$RNOTE_APP_PLIST"

# update minimum system version according to deployment target
if [ -z "$MACOSX_DEPLOYMENT_TARGET" ]; then
  MACOSX_DEPLOYMENT_TARGET=$SYS_SDK_VER
fi
/usr/libexec/PlistBuddy \
  -c "Set LSMinimumSystemVersion $MACOSX_DEPLOYMENT_TARGET" \
  "$RNOTE_APP_PLIST"

# set Rnote version
/usr/libexec/PlistBuddy -c \
  "Set CFBundleShortVersionString '$(rnote_get_version_from_meson)'" \
  "$RNOTE_APP_PLIST"
/usr/libexec/PlistBuddy -c "Set CFBundleVersion '$RNOTE_BUILD'" \
  "$RNOTE_APP_PLIST"

# set copyright
/usr/libexec/PlistBuddy -c "Set NSHumanReadableCopyright 'Copyright © \
$(date +%Y) Felix Zwettler'" "$RNOTE_APP_PLIST"

# set app category
/usr/libexec/PlistBuddy -c "Add LSApplicationCategoryType string \
'public.app-category.graphics-design'" "$RNOTE_APP_PLIST"

# set folder access descriptions
/usr/libexec/PlistBuddy -c "Add NSDesktopFolderUsageDescription string \
'Rnote needs your permission to access the Desktop folder.'" "$RNOTE_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSDocumentsFolderUsageDescription string \
'Rnote needs your permission to access the Documents folder.'" "$RNOTE_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSDownloadsFolderUsageDescription string \
'Rnote needs your permission to access the Downloads folder.'" "$RNOTE_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add NSRemoveableVolumesUsageDescription string \
'Rnote needs your permission to access removeable volumes.'" "$RNOTE_APP_PLIST"

# add supported languages
/usr/libexec/PlistBuddy -c "Add CFBundleLocalizations array" \
  "$RNOTE_APP_PLIST"
/usr/libexec/PlistBuddy -c "Add CFBundleLocalizations: string 'en'" \
  "$RNOTE_APP_PLIST" # because there is no en.po file
for locale in "$RNOTE_SRC_DIR"/crates/rnote-ui/po/*.po; do
  /usr/libexec/PlistBuddy -c "Add CFBundleLocalizations: string \
'$(basename -s .po "$locale")'" "$RNOTE_APP_PLIST"
done

# add additional font directory for bundled fonts
/usr/libexec/PlistBuddy -c "Add ATSApplicationFontsPath string \
'share/rnote/fonts/'" "$RNOTE_APP_PLIST"

# add custom build information (RM = rnote_macos)
/usr/libexec/PlistBuddy -c "Add RMGitDescribeSource string \
'$(git -C "$RNOTE_SRC_DIR" describe --tags)'" "$RNOTE_APP_PLIST"

# add some metadata to make CI identifiable
if $CI_GITLAB; then
  for var in PROJECT_NAME PROJECT_URL COMMIT_BRANCH COMMIT_SHA \
    COMMIT_SHORT_SHA JOB_ID JOB_URL JOB_NAME PIPELINE_ID PIPELINE_URL; do
    # use awk to create camel case strings (e.g. PROJECT_NAME to ProjectName)
    /usr/libexec/PlistBuddy -c "Add CI$(
      echo $var | awk -F _ '{
        for (i=1; i<=NF; i++)
        printf "%s", toupper(substr($i,1,1)) tolower(substr($i,2))
      }'
    ) string $(eval echo \$CI_$var)" "$RNOTE_APP_PLIST"
  done
fi
